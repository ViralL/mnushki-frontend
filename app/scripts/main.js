'use strict';

// ready
$(document).ready(function() {

    // anchor
    // $(".anchor").on("click","a", function (event) {
    //     event.preventDefault();
    //     var id  = $(this).attr('href'),
    //         top = $(id).offset().top;
    //     $('body,html').animate({scrollTop: top + 40}, 1000);
    // });
    // anchor

    // adaptive menu
    function mobileToggle(toggleJs, btnJs, bodyJs, mainJs) {
        $(toggleJs).click(function () {
            //$(this).next().slideToggle();
            $(this).parents().find(bodyJs).toggleClass('active');
            $(this).parent().toggleClass('active');
            $(this).parents().find(mainJs).toggleClass('active');
            $(this).parents().find('.shadow--js').toggleClass('js');
            $(this).parents().find('body').toggleClass('js');
            return false;
        });
        $(btnJs).click(function () {
            //$(this).next().removeClass('active');
            $(this).parents().find(bodyJs).removeClass('active');
            $(this).parents().find(mainJs).removeClass('active');
            $(this).parents().find('.shadow--js').removeClass('js');
            $(this).parents().find('body').removeClass('js');
            return false;
        });
    }
    mobileToggle('.main-nav__toggle--js', '.mclose-btn--js', '.main-nav__collapse', 'main');
    mobileToggle('.topline--js', '.topline__btn--js', '.topline__body--js', '.main');
    // adaptive menu

    // catalog hide
    $('.catalog-hidden--js').click(function () {
        $(this).hide('slow');
        $(this).parent().parent().next().show('slow');
        return false;
    });
    // catalog hide


    $(".fav-button")
        .mouseenter(function(){$(this).addClass("fav-button--active")})
        .mouseleave(function(){ $(this).removeClass("fav-button--active")})
        .click(function() {$(this).toggleClass('active');});

    var menuMain = $('.menu');

    // sliders
    var sliderMain = $('.single-item');
     sliderMain.slick({
         prevArrow: $('.slider-arrow-left'),
         nextArrow: $('.slider-arrow-right'),
         dots: true,
         //fade: true,
         speed: 0,
         responsive: [
             {
                 breakpoint: 768,
                 settings: {
                     dots: false
                 }
             }
         ]
         //vertical: true
     });
     sliderMain.on('beforeChange', function(event, slick, currentSlide, nextSlide){
         $('.slide-img').removeClass('animated bigHeaderSlideUpOut');
         $('.slide-toy .mainimg').removeClass('animated SlideUpOut');
         $('.slide-toy .shadow').removeClass('animated fadeIn');
         $('.slide-text span').removeClass('animated bigHeaderSlideUpIn');
         $('.slide-link').removeClass('animated fadeIn');
     });
     sliderMain.on('afterChange', function(event, slick, currentSlide, nextSlide){
         $('.slide-img').addClass('animated bigHeaderSlideUpOut');
         $('.slide-toy .mainimg').addClass('animated SlideUpOut');
         $('.slide-toy .shadow').addClass('animated fadeIn');
         $('.slide-text span').addClass('animated bigHeaderSlideUpIn');
         $('.slide-link').addClass('animated fadeIn');
     });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        draggable: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        // dots: true,
        // centerMode: true,
        focusOnSelect: true
    });
    $('.slider-fort').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        asNavFor: '.slider-navt',
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    arrows: false
                }
            }
        ]
    });
    $('.slider-navt').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-fort',
        focusOnSelect: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.slider-product').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        //infinite: false,
        //centerMode: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
        //vertical: true
    });
    //sliders

    //
    $('.menu-dropdown').on('mouseleave',function(){
        menuMain.css('border-bottom-right-radius', '5px');
        menuMain.css('border-bottom-left-radius', '5px');
    });

    function toggleTopSearch() {
        var visible = $('.search').css('display');
        if ( visible == 'none' ) {
            $('.topline-wrapper').fadeOut(400);
            $('.search').fadeIn(350);
            $('.search').addClass('active');
            $('#veil').fadeIn(200);
        } else {
            $('.search').fadeOut(350);
            $('.search').removeClass('active');
            $('#veil').fadeOut(200);
            $('.topline-wrapper').fadeIn(400);
        }
    }


    $('.close-search, .topline-search').on('click', function(){
        toggleTopSearch();
    });

    // catalog
    $('ul#catalog-dropdown').on('click', function(e){
        e.preventDefault();
        var visible = $($('ul#catalog-dropdown li')[1]).css('display');
        var first = $('ul#catalog-dropdown li.title');
        if(visible == 'none') {
            $('ul#catalog-dropdown li').css('display', 'block');
            first.addClass('active');
        } else {
            first.removeClass('active');
            $('ul#catalog-dropdown li').not('.title').css('display', 'none');
        }
    });
    // catalog


    //footer
    $('.footer-acc--title').click(function () {
       $(this).parent().toggleClass('active');
    });
    $('.accordion-title-js').click(function () {
       $(this).parent().toggleClass('active');
    });
    //footer

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone


    // select {select2}
    $('select.topline-drop').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // popup {magnific-popup}
    $('.image-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        },
        zoom: {
            enabled: true,
            duration: 300,
            opener: function(element) {
                return element.parent().find('img');
            }
        }
    });
    // popup


    //busket delete function
    $('.busket__delete-js a').click(function(){
        $(this).parent().parent().fadeOut('slow');
        return false;
    });
    //busket delete function


    // .collapse
    $(".collapse__elem-title a").click(function () {
        $(this).parent().hide().next().show('slow');
        return false;
    });
    // .collapse


    //accordion filter
    $('.accordion .accordion-item-js')
        .click(function(event){
            //event.stopPropagation();
        })
        .filter(':not(.active)')
        .hide();
    $('.accordion .accordion-title-js .accordion-title').click(function(){
        var selfClick = $(this).parent().find('.accordion-item-js:first').is(':visible');
        $(this).parent()
            .find('.accordion-item-js:first')
            .stop(true, true)
            .slideToggle('fast')
            .parent().toggleClass('active')
            .parent().toggleClass('active');
        $(this).parent().find('.accordion-title-js_basket').hide();
        return false;
    });
    $('.show-title-js').click(function(){
        $(this).next().slideToggle('');
    });
    $('.search_btn-js').click(function(){
        $(this).parent().prev().addClass('active');
        return false;
    });
    //accordion filter



    // range slider
    if ($("#rangeSlider").length) {
        var range = document.getElementById('rangeSlider');
        var start = document.getElementById('start');
        var end = document.getElementById('end');
        noUiSlider.create(range, {
            start: [300, 810],
            connect: true,
            step: 10,
            range: {
                'min': 0,
                'max': 1000
            }
        });
        range.noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            if (handle) {
                end.value = Math.round(value);
            } else {
                start.value = Math.round(value);
            }
        });
        end.addEventListener('change', function () {
            range.noUiSlider.set([this.value, null]);
        });
        start.addEventListener('change', function () {
            range.noUiSlider.set([null, this.value]);
        });
    }
    // range slider


    //easyAutocomplete
    var options = {
        data: ["blue", "green", "pink", "red", "yellow"],
        list: {
            match: {
                enabled: true
            }
        },
        theme: "square"
    };
    $("#control").easyAutocomplete(options);

    $('#control').keyup(function() {
        if ($(this).val().length == 0) {
            $('.popular-block').show();
        } else {
            $('.popular-block').hide();
        }
    }).keyup();
    //easyAutocomplete


    //cardColor
    $('.cardColor--js').on('click', '> span', function() {
        $(this).toggleClass('active');
    });
    $('.cardColor--js').on('click', '.cardColor__item--close', function() {
        $(this).parent().find('> span').removeClass('active');
    });
    //cardColor


    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.tabs__caption').on('click', '.pickpoint-list__item:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.radiotab--js').on('click', '.radio input:not(.active)', function() {
        var radTab = $(this).val();
        if(radTab == 'deliver') {
            $(this).closest('.radiotab').find('.delivery-block--trs-1').addClass('active');
            $(this).closest('.radiotab').find('.delivery-block--trs-2').removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-1').addClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-2').removeClass('active');
        } else {
            $(this).closest('.radiotab').find('.delivery-block--trs-1').removeClass('active');
            $(this).closest('.radiotab').find('.delivery-block--trs-2').addClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-1').removeClass('active');
            $(this).closest('.radiotab').find('.radiotab--content-2').addClass('active');
        }
    });
    //tabs


    $(function () {
        $('.check_all').change(function () {
            $(this).closest('.checkbox-inner').find('.chk--js').prop('checked', this.checked);
        });
    });

    //custom scroll
    $(".custom-scroll--js").mCustomScrollbar({
        axis:"y"
    });
    //custom scroll
});
// ready
